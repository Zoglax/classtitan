package com.company;

public class Main {

    public static void main(String[] args) {
        Titan titan1 = new Titan();
        Titan titan2 = new Titan("Omegasilverfish",1000,5000);
        Titan titan3 = new Titan("Spider",2000,7000);
        Titan titan4 = new Titan("Cave Spider Titan",titan3.getStrength()+500,titan3.getHealth()+500);
        Titan titan5 = new Titan("Zombie Titan",3000,50000);
        Titan titan6 = new Titan("Pig Zombie Titan",titan5.getStrength()+500,titan5.getHealth()+5000);
        Titan titan7 = new Titan("Ender Colossus",10000,300000);
        Titan titan8 = new Titan("Creeper Titan",4000,50000);

        System.out.println(titan1.getStringInfo());
        System.out.println(titan2.getStringInfo());
        System.out.println(titan3.getStringInfo());
        System.out.println(titan4.getStringInfo());
        System.out.println(titan5.getStringInfo());
        System.out.println(titan6.getStringInfo());
        System.out.println(titan7.getStringInfo());
        System.out.println(titan8.getStringInfo());

        System.out.println();

        titan2.sleep();
        titan3.eat();
        titan5.roar();
        titan6.roar();
        titan7.eat();
        titan7.roar();
        titan7.sleep();

        System.out.println();

        titan1.setStrength(titan1.getStrength()+200);
        titan3.setStrength(titan3.getStrength()+500);
        titan4.setStrength(titan3.getStrength()+1500);
        titan8.setName("Charged "+titan8.getName());
        titan8.setStrength(titan8.getStrength()*2);
        titan8.setHealth(titan8.getHealth()*2);
        titan7.setStrength(titan7.getStrength()*2);
        titan7.setHealth(titan7.getHealth()+50000);

        System.out.println(titan1.getStringInfo());
        System.out.println(titan2.getStringInfo());
        System.out.println(titan3.getStringInfo());
        System.out.println(titan4.getStringInfo());
        System.out.println(titan5.getStringInfo());
        System.out.println(titan6.getStringInfo());
        System.out.println(titan7.getStringInfo());
        System.out.println(titan8.getStringInfo());


    }
}
