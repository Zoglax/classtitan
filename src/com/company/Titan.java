package com.company;

public class Titan {
    private String name;
    private int strength;
    private double health;

    public Titan()
    {
        name = "unname";
        strength = 0;
        health = 0;
    }

    public Titan(String name, int strength, double health)
    {
        this.name = name;
        this.strength = strength;
        this.health = health;
    }

    public Titan (Titan titan)
    {
        name = titan.name;
        strength = titan.strength;
        health = titan.health;
    }

    public String getStringInfo()
    {
        return "Name: "+name+" Strength: "+strength+" Health: "+health;
    }

    public String getName()
    {
        return name;
    }

    public int getStrength()
    {
        return strength;
    }

    public double getHealth()
    {
        return health;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setStrength(int strength)
    {
        if(strength>=1000 && strength<=100000)
        {
            this.strength = strength;
        }
    }

    public void setHealth(double health)
    {
        if(health>=5000 && health<=500000)
        {
            this.health = health;
        }
    }

    public void roar()
    {
        if((strength>9000 && strength<=11000) && (health>250000 && health<=350000))
        {
            System.out.println(name+": ...@$^@#^%@$^$^$@ROOOO@$%#OOOO@$%^OOOOOOAAAA^@$^@AAAAAAAAR!!!&#^*@&^#*&^#&#..."); //Тип этот рёв настолько сильный что, мол, во вселенной может произойдти писец))
        }
        else
        {
            System.out.println(name + ": ...ROAR!!!...");
        }
    }

    public void sleep()
    {
        if((strength>9000 && strength<=11000) && (health>250000 && health<=350000))
        {
            System.out.println(name+": ...@$^@#^%@..Sleee.ee.ping..&#^*@&^#-*&^-#&#...");
        }
        else
        {
            System.out.println(name + ": ...*Sleeping* Zz-z-z");
        }
    }

    public void eat()
    {
        if((strength>9000 && strength<=11000) && (health>250000 && health<=350000))
        {
            System.out.println(name+": ...@$^@#^%@..EatttInG..&#^*@&^#-*&^-#&#...");
        }
        else
        {
            System.out.println(name + ": Eating :3");
        }
    }

    public String speak(String phrase)
    {
        return phrase;
    }
}
